# ThinkPHP6插件助手

#### 介绍

ThinkPHP6 插件助手
微信/QQ：844888168

#### 安装教程

1. 安装：composer require atuni/think-addons dev-main
2. 更新：composer update atuni/think-addons dev-main
3. 移除：composer remove atuni/think-addons

#### 使用说明

1. 建议使用PHP7.1以上
2. 想怎么用就怎么用

